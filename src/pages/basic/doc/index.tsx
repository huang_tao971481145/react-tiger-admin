import React from 'react'
import { Card, Space } from 'antd'

const StandardPages: React.FC = () => {
  return (
    <Card>
      <Space>My pages doc</Space>
    </Card>
  )
}
export default StandardPages
