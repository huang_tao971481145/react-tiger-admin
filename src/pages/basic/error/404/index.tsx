import { FC } from 'react'
import { Button, Card, Result, notification } from 'antd'
import View from '@/components/View'

const Page404: FC = () => {
  const handleClick = () => {
    notification.success({
      message: '您点击了上一页',
    })
  }
  return (
    <View>
      <Card type="inner" className="app-page-404" title="404">
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
          extra={<Button type="primary" onClick={handleClick}>返回上一页</Button>}
        />
      </Card>
    </View>
  )
}

export default Page404
