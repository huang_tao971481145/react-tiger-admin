import React from 'react'
import CodeMirror from '@uiw/react-codemirror'
import { javascript } from '@codemirror/lang-javascript'
import { dracula } from '@uiw/codemirror-theme-dracula'
import { Card } from 'antd'

const Code = `import React from 'react'
import CodeMirror from '@uiw/react-codemirror'
import { javascript } from '@codemirror/lang-javascript'
import { dracula } from '@uiw/codemirror-theme-dracula'

function App() {
  const onChange = React.useCallback((value: string) => {
    console.log('value:', value)
  }, [])

  return (
    <CodeMirror
      value={Code}
      height="100vh - 200px"
      theme={dracula}
      extensions={[javascript({ jsx: true })]}
      onChange={onChange}
    />
  )
}
export default App
`

function CodeEditor() {
  const onChange = React.useCallback((value: string) => {
    console.log('value:', value)
  }, [])

  return (
    <Card type="inner" title="Codemirror">
      <CodeMirror
        value={Code}
        height="100vh - 200px"
        theme={dracula}
        extensions={[javascript({ jsx: true })]}
        onChange={onChange}
      />
    </Card>

  )
}
export default CodeEditor
