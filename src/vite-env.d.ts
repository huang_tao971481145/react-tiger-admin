/// <reference types="vite/client" />
declare module '@carbon/icons-react'

declare module '*.svg' {
  const content: any
  export default content
}

declare module '*.json' {
  const content: any
  export default content
}

// 水印
declare module 'watermark-plus'
// 画板
declare module 'react-canvas-draw'
// 天气预报
declare module 'react-tencent-weather'

declare interface Window {
  returnCitySN: any
}
