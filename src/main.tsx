import React from 'react'
import ReactDOM from 'react-dom'
import { ConfigProvider } from 'antd'
import zhCN from 'antd/es/locale/zh_CN'
import App from './App'
import { Provider } from 'mobx-react'
import store from '@/store'

import '@/styles/global.less'
import 'uno.css'

// react 17 创建，控制台会报错，暂时不影响使用（菜单折叠时不会出现闪烁）
ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider {...store}>
      <App />
    </Provider>
  </ConfigProvider>,
  document.getElementById('root'),
)

// import ReactDOM from "react-dom/client"
// ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
//   <React.StrictMode>
//     <ConfigProvider locale={zhCN}>
//       <Provider {...store}>
//         <App />
//       </Provider>
//     </ConfigProvider>
//   </React.StrictMode>,
// )
