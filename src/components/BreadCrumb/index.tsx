import { Breadcrumb } from 'antd'
import { useStore } from '@/store'
import { observer } from 'mobx-react'
import './index.less'

const BreadCrumb = () => {
  const { appStore } = useStore()
  const { currentMenuList } = appStore
  const extraBreadcrumbItems = currentMenuList?.map((item: any) => <Breadcrumb.Item key={item.path}>{item.label}</Breadcrumb.Item>)

  return (
    <div className="app-breadcrumb">
      <Breadcrumb style={{ lineHeight: '64px' }} separator=">">{extraBreadcrumbItems}</Breadcrumb>
    </div>
  )
}

export default observer(BreadCrumb)
