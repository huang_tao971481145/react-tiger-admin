import { Button, Space, Tooltip } from 'antd'
import { SearchOutlined, ReloadOutlined, UnorderedListOutlined } from '@ant-design/icons'

const TableToolBar = (props: any) => {
  const { toggleSearch, refreshData, showColumn = () => {} } = props
  const onSearch = toggleSearch
  const onRefresh = refreshData
  const onColumn = showColumn

  return (
    <div className="app-table-action-button">
      <Space size={15}>
        <Tooltip title="搜索">
          <Button shape="circle" icon={<SearchOutlined />} onClick={() => onSearch()} />
        </Tooltip>
        <Tooltip title="刷新">
          <Button shape="circle" icon={<ReloadOutlined />} onClick={() => onRefresh()} />
        </Tooltip>
        <Tooltip title="隐藏列">
          <Button shape="circle" icon={<UnorderedListOutlined />} onClick={() => onColumn()} />
        </Tooltip>
      </Space>
    </div>
  )
}

export default TableToolBar
