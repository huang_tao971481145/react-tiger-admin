import React, { FC } from 'react'
import classnames from 'classnames'
import styles from './view.module.less'

export type IProps = {
  title?: string
  className?: string
  loading?: boolean
  inner?: boolean
  extra?: any
  children?: React.ReactNode
}

const View: FC<IProps> = ({ title = '', className, children, loading = false, inner = false, extra }) => {
  const loadingStyle = {
    height: 'calc(100vh - 184px)',
    overflow: 'hidden',
  }

  return (
    <div className="app-view">
      {title && (
        <div className={styles.appViewTitle}>
          <h1>{title}</h1>
          {extra || null}
        </div>
      )}
      <div
        className={classnames(className, {
          [styles.contentInner]: inner,
        })}
        style={loading ? loadingStyle : {}}
      >
        {children}
      </div>
    </div>
  )
}

export default View
