import { lazy } from 'react'
import lazyLoad from '@/router/utils/lazyLoad'
import { CodeOutlined, BlockOutlined, FileWordOutlined, FileMarkdownOutlined, ScissorOutlined } from '@ant-design/icons'

export default [{
  path: '/components',
  key: '/components',
  label: '组件',
  title: '组件',
  icon: <BlockOutlined />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  children: [
    {
      label: '富文本',
      title: '富文本',
      icon: <FileWordOutlined />,
      path: '/components/tinymce',
      key: '/components/tinymce',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "tinymce" */ '@/pages/components/tinymce'))),
    },
    {
      label: 'Markdown',
      title: 'Markdown',
      icon: <FileMarkdownOutlined />,
      path: '/components/markdown',
      key: '/components/markdown',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "markdown" */ '@/pages/components/markdown'))),
    },
    {
      label: 'Codemirror',
      title: 'Codemirror',
      icon: <CodeOutlined />,
      path: '/components/codemirror',
      key: '/components/codemirror',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "Codemirror" */ '@/pages/components/codemirror'))),
    },
    {
      label: 'Clipboard',
      title: 'Clipboard',
      icon: <ScissorOutlined />,
      path: '/components/clipboard',
      key: '/components/clipboard',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "clipboard" */ '@/pages/components/clipboard'))),
    },
  ],
}]
